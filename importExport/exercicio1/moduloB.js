const cliente = {id: 3, nome: 'Joana', idade: 22}
const valor = 1000 // Como não foi exportado, só é acessível aqui neste módulo

module.exports = { 
    cliente,
    bomDia: 'Bom dia',
    boaNoite: () => 'Boa noite',
    obj: {id: 1, name: 'Rafael'}
}