// importando informações de outros arquivos js
const importandoA = require('./moduloA')
const importandoB = require('./moduloB')

console.log(importandoA, importandoB) // estas constantes contém um objeto com os itens importados

//console.log(importandoA.ateMais)
//console.log(importandoB.bomDia)

const {cliente: client, boaNoite: fala} = importandoB // destructuring
console.log(client)