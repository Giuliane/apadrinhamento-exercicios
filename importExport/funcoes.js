const bestRockBand = (band) => {
    return new Promise((resolve, reject) => {
        if(band === 'Green Day'){
            resolve({
                sucess: true, 
                bandName: band, 
                msg: `${band} is the best rock band!`
            })
        }
        reject({
            sucess: false, 
            bandName: band,
            msg: `${band}? i'm not so sure..`
        })
    })
}

const bestRockSong = (response) => { // pega como param o objeto gerado na promise anterior, pq é encadeado
    return new Promise((resolve, reject) => {
        if(response.sucess){
            resolve(`American Idiot by ${response.bandName}`)
        }
        reject('Do you know Green Day?')
    })
}

const best = async () => { // precisa colocar num bloco try/catch pra tratar possiveis erros
    try {
        const bestRockBandResponse = await bestRockBand('Green Day')
        console.log(bestRockBandResponse) // .msg mostra só a frase
        const bestRockSongResponse = await bestRockSong(bestRockBandResponse)
        console.log(bestRockSongResponse)
        setTimeout(() => {
            console.log('Timeout Finished')           
        }, 2000);
        console.log('Testando quando será impresso') 
    } catch (error) {
        console.log(error.msg)
    }
}

module.exports = {bestRockBand, bestRockSong, best} // exportando funções