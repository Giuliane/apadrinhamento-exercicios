// Vídeo para dar uma ajudinha na explicação: https://www.youtube.com/watch?v=nRJhc6vXyK4&ab_channel=C%C3%B3digoFonteTV

const soma = () => {
    const resultado = 1 + 1
    return new Promise ((resolve, reject) => {
        if(resultado === 2){
            resolve('Soma com resultado 2.')
        }
        else{
            reject('Valor inesperado.') 
        }
    })
}

let prom = (res) => new Promise((resolve, reject) => {
    //const res = 4 * 5
    if(res === 20){
        resolve('Multiplicação com resultado 20.')
    }
    reject('Valor inesperado.')
})
//Promise.all(soma(), prom)

prom(20)
    .then((result) => {
        console.log(result)
    })
    .catch((err) => { // tratar erros
        console.log(err)
    });