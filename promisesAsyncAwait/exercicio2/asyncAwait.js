// Mesmo exercício do callbackHell.js e do promises.js porém agora utilizando async await
const http = require('http')

const getTurma = (letra) => {
    const url = `http://files.cod3r.com.br/curso-js/turma${letra}.json` // Link contém um array de objetos, quero todos os nomes dos alunos de todas as turmas A, B e C
    return new Promise((resolve, reject) => {
        http.get(url, res => {
            let resultado = ''
        
            res.on('data', dados => {
              resultado += dados
            })
        
            res.on('end', () => {
                try {
                    resolve(JSON.parse(resultado))
                } catch (error) {
                    reject(error)
                }

            })
        })
    })
}

// O objetivo do async/await é simplificar o uso de promise 
let obterAlunos = async () => {
    const turmaA = await getTurma('A') // com async await a função só vai pro proximo passo quando resolvida ou rejeitada
    const turmaB = await getTurma('B') // proximo passo..
    const turmaC = await getTurma('C')
    return [].concat(turmaA, turmaB, turmaC)
} // retorna um async function

obterAlunos()
    .then(a => a.map(a => a.nome))
    .then(nomes => console.log(nomes))

