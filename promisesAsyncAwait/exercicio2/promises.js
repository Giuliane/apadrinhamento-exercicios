// Mesmo exercício do callbackHell.js mas agora utilizando promises
const http = require('http')

const getTurma = (letra) => {
    const url = `http://files.cod3r.com.br/curso-js/turma${letra}.json` // Link contém um array de objetos, quero todos os nomes dos alunos de todas as turmas A, B e C
    return new Promise((resolve, reject) => {
        http.get(url, res => {
            let resultado = ''
        
            res.on('data', dados => {
              resultado += dados
            })
        
            res.on('end', () => {
                try {
                    resolve(JSON.parse(resultado))
                } catch (error) {
                    reject(error)
                }
           
            })
        })
    })
}

// Promise.race é o mesmo conceito da .all, mas retorna apenas uma promise, a primeira a ser resolvida/rejeitada
Promise.all([getTurma('A'), getTurma('B'), getTurma('C')]) // chama as promises num array e espera todas serem resolvidas ou rejeitadas
    .then(turmas => [].concat(...turmas)) // junta tudo em um unico vetor
    .then(alunos => alunos.map(aluno => aluno.nome))
    .then(x => console.log(x)) //then manipula o que te foi retornado pela promise
    .catch(e => console.log((`Erro do catch ${e.message}`))) // tratamento de erro