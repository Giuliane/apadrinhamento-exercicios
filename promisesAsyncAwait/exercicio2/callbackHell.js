const http = require('http')

const getTurma = (letra, callback) => {
    const url = `http://files.cod3r.com.br/curso-js/turma${letra}.json` // Link contém um array de objetos, quero todos os nomes dos alunos de todas as turmas A, B e C
    http.get(url, res => {
    let resultado = ''

    res.on('data', dados => {
        resultado += dados
    })

    res.on('end', () => {
        callback(JSON.parse(resultado))
    })

  })
}

let nomes = [] // Vai receber os nomes de todos os alunos das turmas
getTurma('A', alunos => {
    nomes = nomes.concat(alunos.map(a => `A: ${a.nome}`))

    getTurma('B', alunos => {
        nomes = nomes.concat(alunos.map(a => `B: ${a.nome}`))

        getTurma('C', alunos => {
            nomes = nomes.concat(alunos.map(a => `C: ${a.nome}`))
            console.log(nomes)
        })

    })  

})