/* Vídeo para dar uma ajudinha na explicação :)
   https://www.youtube.com/watch?v=h0sNAXE1ozo&ab_channel=C%C3%B3digoFonteTV
   Dica: tente adaptar/personalizar essas funções conforme você quiser!
*/
const bestRockBand = (band) => {
    return new Promise((resolve, reject) => {
        if(band === 'Green Day'){
            resolve({
                sucess: true, 
                bandName: band, 
                msg: `${band} is the best rock band!`
            })
        }
        reject({
            sucess: false, 
            bandName: band,
            msg: `${band}? i'm not so sure..`
        })
    })
}


const bestRockSong = (response) => { // pega como param o objeto gerado na promise anterior, pq é encadeado
    return new Promise((resolve, reject) => {
        if(response.sucess){
            resolve(`American Idiot by ${response.bandName}`)
        }
        reject('Do you know Green Day?')
    })
}
/* SEM ASYNC AWAIT, APENAS COM PROMISES:
bestRockBand('Green Day')
    .then(responseObj => {
        console.log('Checking the answer...')
        return bestRockSong(responseObj)
    })
    .then(responseString => {
        console.log('Finding the best song...')
        console.log(responseString)
    })
    .catch(e => console.log(e.msg))
*/

// COM ASYNC AWAIT:
const best = async () => { // precisa colocar num bloco try/catch pra tratar possiveis erros
    try {
        const bestRockBandResponse = await bestRockBand('Green Day')
        console.log(bestRockBandResponse) // .msg mostra só a frase
        const bestRockSongResponse = await bestRockSong(bestRockBandResponse)
        console.log(bestRockSongResponse)
        setTimeout(() => {
            console.log('Timeout Finished')           
        }, 2000);
        console.log('Testando a ordem que imprime') 
    } catch (error) {
        console.log(error.msg)
    }
}
best()
