# Apadrinhamento 

## Materiais úteis 
<p align="justify"> Este repositório contém material para auxiliar no processo de aprendizado dos novos navers, quem quiser contribuir com novos materiais sinta-se à vontade :) </p>

#### Documentação mozila javascript :computer:
*https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/Guide*

#### Sugestões de cursos 
**Udemy:** *https://www.udemy.com/course/curso-web/*

**Youtube:** *https://www.youtube.com/watch?v=BXqUH86F-kA&list=PLntvgXM11X6pi7mW0O4ZmfUI1xDSIbmTm&ab_channel=CursoemV%C3%ADdeo*

#### Javascript Assíncrono: callbacks, promises e async await:
**Artigos:** *https://medium.com/@alcidesqueiroz/javascript-ass%C3%ADncrono-callbacks-promises-e-async-functions-9191b8272298*

**Vídeos:**

Promises: *https://www.youtube.com/watch?v=nRJhc6vXyK4&ab_channel=C%C3%B3digoFonteTV*

Async Await: *https://www.youtube.com/watch?v=h0sNAXE1ozo&ab_channel=C%C3%B3digoFonteTV*


